<?php


namespace UFXDCollective\URLRedirection\Providers;


use Illuminate\Support\ServiceProvider;

class URLRedirectionServiceProvider extends ServiceProvider
{


    public function version6_OrGreater(){
        return version_compare($this->app->version(), '6.0', '>');
    }


    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){

        $this->loadMigrationsFrom(__DIR__.'/../../migrations');

        $group = 'ufxdcollective_url_redirection_migrations';
        if($this->version6_OrGreater()){
            $group = [
                'ufxdcollective', 'ufxdcollective_url_redirection',
                'ufxdcollective_url_redirection_migrations',
                'migrations'
            ];
        }

        $this->publishes([
            __DIR__.'/../../migrations' => database_path('migrations'),
        ], $group);

    }


    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function register()
    {



    }


}
