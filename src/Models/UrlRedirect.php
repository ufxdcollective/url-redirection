<?php

namespace UFXDCollective\URLRedirection\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * @property int $id
 * @property string $from_url
 * @property string $to_url
 * @property int $method
 * @property bool $is_enabled
 */
class UrlRedirect extends Model
{


    public $timestamps = false;


    protected $fillable = [
        'from_url', 'to_url', 'method', 'is_enabled'
    ];


    protected $casts = [
        'is_enabled' => 'boolean',
        'method' => 'integer',
    ];


    public function scopeEnabled(Builder $query){
        $query->where('is_enabled', true);
    }


    public function scopeFromUrl(Builder $query, $url){
        $query->where('from_url', $url);
    }


    public function scopeSearch(Builder $query, $search = []){

        if(isset($search['keywords']) AND !empty($search['keywords'])){

            $keywords = Str::slug($search['keywords'], '%');
            $keywords = "%$keywords%";

            $query->where(function(Builder $query) use ($keywords){
                $query->where('from_url', 'LIKE', $keywords);
                $query->orWhere('to_url', 'LIKE', $keywords);
            });

        }

        if(isset($search['method']) AND !empty($search['method'])){
            $method = $search['method'];
            $query->where('method', $method);
        }

        if(isset($search['is_enabled'])){
            $is_enabled = intval($search['is_enabled']);
            $query->where('is_enabled', $is_enabled);
        }


    }


}
