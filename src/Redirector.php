<?php


namespace UFXDCollective\URLRedirection;

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use UFXDCollective\URLRedirection\Models\UrlRedirect;
use Illuminate\Support\Facades\Request as RequestFacade;

class Redirector
{


    /**
     * Gets the url without the root.
     *
     * @param string $url
     * @return string
     */
    public static function filterURL(string $url = NULL): string {
        $url = $url ?? RequestFacade::fullUrl();
        $url = preg_replace(['/(\?.*)\=\&/i', '/(\?.*)\=$/i'], ['$1&', '$1'], $url);
        return str_replace(RequestFacade::root(), '', $url);
    }


    public static function isHttpException(\Exception $exception){
        return $exception instanceof HttpExceptionInterface;
    }


    /**
     * @param $request
     * @param \Exception $exception
     * @return \Illuminate\Http\RedirectResponse|null
     */
    public static function redirects(Request $request, \Exception $exception){

        if( static::isHttpException($exception) AND $exception->getStatusCode() == 404 ){

            /** @var UrlRedirect $redirect */
            $redirect = UrlRedirect::query()
                ->fromUrl(static::filterURL())
                ->enabled()
                ->first();

            if($redirect){
                return redirect()
                    ->to($redirect->to_url)
                    ->setStatusCode($redirect->method);
            }

        }

        return NULL;
    }

}
