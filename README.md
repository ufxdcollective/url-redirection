# URL Redirection

A simple Laravel module to redirect users if landed to an unavailable URL.

Use this module for SEO purposes or present user to a new URL that represents an old non-existing (anymore) URL. The redirected can be any of internal or external URLs. So, you may also use this
to shorten long urls.



## How to enable Redirection Module.

First you need to install this package using composer and
then update a bit of you application code to enable it. Note
that this package has Laravel auto-discovery enabled. So,
you don't need to worry about loading the service provider. 



### Install Package

In your project root directory run this command to install
it into your project. We are using composer to do the job.

```sh
composer require ufxdcollective/url-redirection
```



### Migrate required table into database.

Run this command to migrate the database table(s) required for
this package to work.

```sh
php artisan migrate
```

You may also publish the migrations and update if you require.


### Setup

Update the `render(...)` method in file `app/Exceptions/Handler.php`
as shown below. The rest will be taken care by the Redirection
Module itself.


```php

public function render($request, Exception $exception)
{

    // Redirection Module ------------------
    $redirect = Redirector::redirects($request, $exception);
    if($redirect) return $redirect;
    // --------------------------------------------

    return parent::render($request, $exception);
}

```



### NOTE

This package don't ship with views or controllers to save the
redirection maps into database. So, for an interface, you must
do the job yourself.

Don't forget to filter the `from_url` using
`Redirector::filterURL($url)` before saving in into the
database.


Here is a code snippet of now you may use model to store the
url redirect mappings.

```php

use \UFXDCollective\URLRedirection\Redirector;
use \UFXDCollective\URLRedirection\Models\UrlRedirect;


// ...
// ...

$from_url = $request->input('from_url');

// Filter $from_url for better url consistency
$from_url = Redirector::filterURL($from_url);

// You don't need to filter the $to_url
$from_url = $request->input('from_url'); 

$urlRedirect = new UrlRedirect([
    'from_url' => $from_url,
    'to_url' => $to_url,
    'method' => 301,
    'is_enabled' => true,
]);


$urlRedirect->save();

// ...


```
